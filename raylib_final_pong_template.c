/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    const int maxSpeed = 8;
    const int minSpeed = 8;
    GameScreen screen = LOGO;
   
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    bool pause = false;
    
    
    
    Rectangle enemy;
   
    enemy.width = 20;
    enemy.height = 100;    
    enemy.x = screenWidth - 50 - enemy.width;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 8;
    
    Rectangle player;
    player.width = 20;
    player.height = 100;    
    player.x = 50; 
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY =8;
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    int ballRadius =25;
    
    Vector2 ballSpeed;
    ballSpeed.x = minSpeed;
    ballSpeed.y = minSpeed;
    
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    int playerLife;
    int enemyLife;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    Font fontTtf = LoadFontEx("Resources/KOMIKAX_.ttf", 32, 0, 250);
    
    Texture2D Logo = LoadTexture("Resources/Pong_logo.png");
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
               DrawTextEx(fontTtf, "Press T to continue", (Vector2){screenWidth/2 - MeasureText("Press T to continue", 70)/2, screenHeight/2}, 20, 1, RED);// Update LOGO screen data here!
               if (IsKeyPressed(KEY_T)){
          screen = TITLE;
        }
               //Dibuja el texto
               
               
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                DrawTextEx(fontTtf, "FINAL PONG", (Vector2){screenWidth/2 - MeasureText("FINAL PONG", 40)/2, screenHeight/2}, 50, 5, RED);
                DrawTextEx(fontTtf, "PRESS ENTER TO BEGIN", (Vector2){screenWidth/2 - MeasureText("PRESS ENTER TO BEGIN", 25)/2, 200}, 30, 5, RED);
                // TODO: Title animation logic.......................(0.5p)
                if (IsKeyPressed(KEY_ENTER)){
          screen = GAMEPLAY;
        }
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                 // TODO: Player movement logic.......................(0.2p)
                 if(!pause){
            if (IsKeyDown(KEY_Q)){
              player.y -= playerSpeedY;
               //iaLinex+=velocidady;
            }
           
            if (IsKeyDown(KEY_A)){
                //iaLinex -=velocidady;
              player.y += playerSpeedY;
            }
                // TODO: Ball movement logic.........................(0.2p) 
                if(!pause){
            ball.x += ballSpeed.x;
            ball.y += ballSpeed.y;
                }
            }
             // TODO: Collision detection (ball-limits) logic.....(1p)
            if((ball.y > screenHeight - ballRadius) || (ball.y < ballRadius) ){
            ballSpeed.y *=-1;
            //PlaySound(fxWav);
        }
 
            if(ball.x > screenWidth - ballRadius){
           
            //PlaySound(fxGoal);
            ball.x = screenWidth/2;
            ball.y = screenHeight/2;
            ballSpeed.x = -minSpeed;
            ballSpeed.y = minSpeed;
           
            }else if(ball.x < ballRadius){
            
            //PlaySound(fxGoal);
                ball.x = screenWidth/2;
                ball.y = screenHeight/2;
                ballSpeed.x = minSpeed;
                ballSpeed.y = minSpeed;
            }
 
            // TODO: Collision detection (ball-player) logic.....(0.5p) 
            if(CheckCollisionCircleRec(ball, ballRadius, player)){
            if(ballSpeed.x>0){
                //PlaySound(fxWav);
                if(abs(ballSpeed.x)<maxSpeed){                    
                    ballSpeed.x *=-1.5;
                    ballSpeed.y *= 1.5;
                }else{
                    ballSpeed.x *=-1;
                }
            }
        }
            // TODO: Collision detection (ball-enemy) logic......(0.5p)
            if(CheckCollisionCircleRec(ball, ballRadius, enemy)){
            if(ballSpeed.x<0){
                //PlaySound(fxWav);
                if(abs(ballSpeed.x)<maxSpeed){                    
                    ballSpeed.x *=-1.5;
                    ballSpeed.y *= 1.5;
                }else{
                    ballSpeed.x *=-1;
                }
            }
        }   // TODO: Enemy movement logic (IA)...................(1p)
           
                
               
            
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P)){
          pause = !pause;
        }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here
                    
                    DrawTexture(Logo, 200, 50, WHITE);
                    
                   
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    DrawTextEx(fontTtf, "FINAL PONG", (Vector2){screenWidth/2 - MeasureText("FINAL PONG", 40)/2, screenHeight/2}, 50, 5, RED);
                    // TODO: Draw Title..............................(0.2p)
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    ClearBackground(BLACK);

                    DrawRectangleRec(player, GREEN);
       
                    DrawRectangleRec(enemy, GREEN);
           
                    DrawCircleV(ball, ballRadius, GREEN);
                     
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if(pause){
                DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 0, 255, 0, 255/2 });  
            DrawTextEx(fontTtf, "Press p to continue", (Vector2){screenWidth/2 - MeasureText("Press p to continue", 35)/2, screenHeight/2}, 40, 5, BLACK);
            }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}